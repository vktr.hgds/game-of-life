package com.hegedusviktor;

public interface GameBoardMethods {

    void run ();
    void setStartingPosition ();
    int countNeighbours(Position pos);
    int countSymbols();
    void showMap();
}
