package com.hegedusviktor;

import java.util.Scanner;

public class GameBoard implements GameBoardMethods {

    private char[][] map;
    private char[][] dead;
    private char[][] newBorn;

    private int height;
    private int width;
    private char symbol;
    private int numberOfSymbols;

    private char deadSymbol = '#';
    private char newBornSymbol = '@';

    private Scanner scanner = new Scanner(System.in);
    private static final String WHITE = "\033[0;33m";
    private static final String ANSI_YELLOW_BACKGROUND = "\033[0;36m";
    private static final String ANSI_RESET = "\033[0m";


    public GameBoard (int height, int width, char symbol, int numberOfSymbols) {
        checkMapSize(height, width);
        this.height = height;
        this.width = width;
        this.symbol = symbol;
        this.numberOfSymbols = numberOfSymbols;
        this.map = initializeMap(this.map);
        this.dead = initializeMap(this.dead);
        this.newBorn = initializeMap(this.newBorn);
    }


    private char[][] initializeMap (char[][] board) {
        board = new char[height][width];
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                board[i][j] = '0';
            }
        }
        return board;
    }


    private void chooseStartingPosition (int x, int y) {
        try {
            checkMapParams(x, y);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.map[x][y] = (char) symbol;
    }


    public void setStartingPosition () {
        for (int i = 0; i < this.numberOfSymbols; i++) {
            System.out.print((i + 1) + ". Enter height value:  ");
            int x = scanner.nextInt();
            scanner.nextLine();

            System.out.print((i + 1) + ". Enter width value: ");
            int y = scanner.nextInt();
            scanner.nextLine();
            chooseStartingPosition(x, y);
        }
        System.out.println();
    }


    public void run () {
        generateSymbols();
        generateNewBorns();
        killSymbols();
        sleep(100);
        System.out.println("Symbols: " + countSymbols());
    }


    private void generateSymbols () {

        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                int neighbours = countNeighbours(new Position(i, j));
                if (this.map[i][j] == this.symbol) {
                    if (neighbours != 2 && neighbours != 3) {
                        this.dead[i][j] = this.deadSymbol;
                    }
                }
                else if (this.map[i][j] == '0'){
                    if (neighbours == 3) {
                        this.newBorn[i][j] = this.newBornSymbol;
                    }
                }
            }
        }
    }

    private void generateNewBorns () {
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                if (this.newBorn[i][j] == this.newBornSymbol) {
                    this.map[i][j] = this.symbol;
                }
            }
        }
        this.newBorn = initializeMap(this.newBorn);
    }


    private void killSymbols () {
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                if (this.dead[i][j] == this.deadSymbol) {
                    this.map[i][j] = '0';
                }
            }
        }
        this.dead = initializeMap(this.dead);
    }


    private void checkMapSize (int x, int y) {
        if (x <= 10) x = 10;
        else if (x > 50) x = 50;
        if (y <= 10) y = 10;
        else if (y > 50) y = 50;
    }


    private boolean alreadyChosen (int x, int y) {
        return this.map[x][y] == this.symbol;
    }


    private void sleep (long length) {
        try {
            Thread.sleep(length);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void checkMapParams (int x, int y) throws Exception {
        if (x >= this.height || x < 0) {
            throw new Exception("Error, invalid 'height' value");
        }
        if (y >= this.width || y < 0) {
            throw new Exception("Error, invalid 'width' value");
        }
    }


    public void showMap () {
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                if (this.map[i][j] == this.symbol) {
                    System.out.print(ANSI_YELLOW_BACKGROUND + this.map[i][j] + " " + ANSI_RESET);
                } else {
                    System.out.print(WHITE + this.map[i][j] + " " + ANSI_RESET);
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    public void showNewBorn() {
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                System.out.print(WHITE + this.map[i][j] + " " + ANSI_RESET);
            }
        }
        System.out.println();
    }


    public int countSymbols () {
        int count = 0;
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                if (map[i][j] == this.symbol) count++;
            }
        }
        return count;
    }


    public int countNeighbours(Position pos) {
        int neighbours = 0;

        Position upperLeftDiag = new Position(pos.getX() - 1, pos.getY() - 1);
        Position lowerLeftDiag = new Position(pos.getX() + 1, pos.getY() - 1);

        Position upperRightDiag = new Position(pos.getX() - 1, pos.getY() + 1);
        Position lowerRightDiag = new Position(pos.getX() + 1, pos.getY() + 1);

        Position above = new Position(pos.getX() - 1, pos.getY());
        Position under = new Position(pos.getX() + 1, pos.getY());

        Position left = new Position(pos.getX(), pos.getY() - 1);
        Position right = new Position(pos.getX(), pos.getY() + 1);

        if (upperLeftDiag.getY() < 0) upperLeftDiag.setY(this.width - 1);
        if (upperLeftDiag.getX() < 0) upperLeftDiag.setX(this.height - 1);

        if (lowerLeftDiag.getY() < 0) lowerLeftDiag.setY(this.width - 1);
        if (lowerLeftDiag.getX() >= this.height - 1) lowerLeftDiag.setX(0);

        if (upperRightDiag.getY() >= this.width) upperRightDiag.setY(0);
        if (upperRightDiag.getX() < 0) upperRightDiag.setX(this.height - 1);

        if (lowerRightDiag.getY() >= this.width) lowerRightDiag.setY(0);
        if (lowerRightDiag.getX() >= this.height) lowerRightDiag.setX(0);

        if (above.getX() < 0) above.setX(this.height - 1);
        if (under.getX() >= this.height) under.setX(0);

        if (left.getY() < 0) left.setY(this.width - 1);
        if (right.getY() >= this.width) right.setY(0);

        if (this.map[upperLeftDiag.getX()][upperLeftDiag.getY()] == this.symbol) neighbours++;
        if (this.map[lowerLeftDiag.getX()][lowerLeftDiag.getY()] == this.symbol) neighbours++;

        if (this.map[upperRightDiag.getX()][upperRightDiag.getY()] == this.symbol) neighbours++;
        if (this.map[lowerRightDiag.getX()][lowerRightDiag.getY()] == this.symbol) neighbours++;

        if (this.map[above.getX()][above.getY()] == this.symbol) neighbours++;
        if (this.map[under.getX()][under.getY()] == this.symbol) neighbours++;

        if (this.map[left.getX()][left.getY()] == this.symbol) neighbours++;
        if (this.map[right.getX()][right.getY()] == this.symbol) neighbours++;

        return neighbours;
    }


    //Used for testing
    public void hasSameElement () {
        for (int i = 0; i < this.height; i++) {
            for (int j = 0; j < this.width; j++) {
                if (this.dead[i][j] == this.deadSymbol && this.newBorn[i][j] == this.newBornSymbol) {
                    System.out.println(i + "+" +j + ": same");
                }
            }
        }
    }
}
