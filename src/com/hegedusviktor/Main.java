package com.hegedusviktor;

public class Main {

    public static void main(String[] args) {
        start(12, 12, '*', 5, 30);
    }

    private static void start (int height, int width, char symbol, int numberOfSymbols, int maxIterations) {
        GameBoard gameBoard = new GameBoard(height, width, symbol,numberOfSymbols);
        gameBoard.showMap();
        gameBoard.setStartingPosition();
        gameBoard.showMap();

        int iterations = 0;
        while (gameBoard.countSymbols() > 0 && iterations < maxIterations) {
            gameBoard.run();
            gameBoard.showMap();
            iterations++;
        }

    }
}
